<?php /** Created by Anton on 15.07.2020. */

return [
    'empty' => 'У Вас нет пластинок',
    'caption' => 'Пластинки',
    'name' => 'Название',
    'date' => 'Дата',
    'author' => 'Автор',
    'actions' => 'Действия',
    'create header' => 'Добавление пластинки',
    'edit header' => 'Редактирование<br /> пластинки',
    'save success' => 'Пластинка добавлена',
    'delete success' => 'Пластинка удалена',
    'edit success' => 'Пластинка изменена',
    'not found' => 'Пластинка не найдена'
];
