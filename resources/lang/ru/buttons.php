<?php /** Created by Anton on 15.07.2020. */

return [
    'edit' => 'Редактировать',
    'delete' => 'Удалить',
    'add' => 'Добавить',
    'save' => 'Сохранить'
];
