@if($errors->any())
    <div class="row mt-4">
        <div class="alert alert-danger m-auto">
            <ul class="mb-0">
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
