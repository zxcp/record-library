@extends('layouts.app')

@section('title', 'Records')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-10 col-sm-8 col-md-6 col-lg-4 m-auto">
                <form action="/create" method="POST" class="form">
                    @csrf
                    <h1 class="h3 mb-3 font-weight-normal text-center">
                        {{ __('record.create header') }}
                    </h1>
                    <div class="form-group">
                        <label class="sr-only" for="iName">{{ __('record.name') }}</label>
                        <input name="name" class="form-control" type="text" placeholder="{{ __('record.name') }}"
                               id="iName" value="{{ old('name') }}" autofocus>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="iDate">{{ __('record.date') }}</label>
                        <input name="date" class="form-control datepicker" type="text" value="{{ old('date') }}"
                               placeholder="{{ __('record.date') }}" id="iDate" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="iAuthor">{{ __('record.author') }}</label>
                        <input name="author" class="form-control" type="text" placeholder="{{ __('record.author') }}"
                               id="iAuthor" value="{{ old('author') }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block">
                            {{ __('buttons.add') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        @include('errors.block')
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush
