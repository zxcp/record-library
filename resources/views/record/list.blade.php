@extends('layouts.app')

@section('title', 'Records')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-sm-10 offset-lg-2 offset-sm-1">
                @if(session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @empty($records)
                    <h2 class="text-center">{{ __('record.empty') }}</h2>
                @else
                    <table class="table">
                        <caption>{{ __('record.caption') }}</caption>
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>{{ __('record.name') }}</td>
                                <td>{{ __('record.date') }}</td>
                                <td>{{ __('record.author') }}</td>
                                <td>{{ __('record.actions') }}</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($records as $record)
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>{{ $record->name }}</td>
                                    <td>{{ $record->getDate() }}</td>
                                    <td>{{ $record->author }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="/edit?recordId={{ $record->id }}" class="btn btn-sm btn-secondary">{{ __('buttons.edit') }}</a>
                                            <a href="/delete?recordId={{ $record->id }}" class="btn btn-sm btn-danger">{{ __('buttons.delete') }}</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endempty
            </div>
        </div>
        <div class="row">
            <a href="{{ route('new') }}" class="btn btn-primary m-auto">{{ __('buttons.add') }}</a>
        </div>
        <div class="row mt-5">
            <div class="m-auto">
                {{ $records->links() }}
            </div>
        </div>
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endsection
