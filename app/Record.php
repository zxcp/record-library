<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Record Пластинка
 * @package App
 *
 * @property int $id
 * @property string $name
 * @property string $date
 * @property string $author
 * @property int $user_id
 */
class Record extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'author'];

    public function getDate()
    {
        return Carbon::createFromFormat('Y-m-d', $this->date)->format('d.m.Y');
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date->format('Y-m-d');
    }

    public function setDateFromString(string $date)
    {
        $date = new \DateTime($date);
        $this->date = $date->format('Y-m-d');
    }
}
