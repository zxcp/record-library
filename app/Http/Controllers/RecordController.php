<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRequest;
use App\Http\Requests\UpdateRequest;
use App\Record;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class RecordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Список пластинок пользователя
     */
    public function list()
    {
        $records = Record::orderBy('id')
            ->where(['user_id' => Auth::id()])
            ->paginate(7)
        ;

        return view('record.list', compact('records'));
    }

    /**
     * Форма добавления новой пластинки
     */
    public function createForm()
    {
        return view('record.create');
    }

    /**
     * Форма редактирования пластинки
     *
     * @param Request $request
     *
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $record = $this->getUserRecord((int)$request->get('recordId'));

        return view('record.edit', compact('record'));
    }

    /**
     * Создание новой пластинки
     *
     * @param CreateRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function create(CreateRequest $request)
    {
        $requestData = $request->validationData();

        $record = new Record($requestData);
        $record->user_id = Auth::id();
        $record->setDateFromString($requestData['date']);
        $record->save();

        return redirect('list')->with('success', Lang::get('record.save success'));
    }

    /**
     * Удаление пластинки
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     *
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        $record = $this->getUserRecord((int)$request->get('recordId'));
        $record->delete();

        return redirect('list')->with('success', Lang::get('record.delete success'));
    }

    /**
     * Редактирование пластинки
     *
     * @param UpdateRequest $request
     *
     * @return array|\Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View|void
     */
    public function update(UpdateRequest $request)
    {
        $requestData = $request->validationData();

        $record = $this->getUserRecord($requestData['recordId']);
        $record->fill($requestData);
        $record->setDateFromString($requestData['date']);
        $record->save();

        return redirect('list')->with('success', Lang::get('record.edit success'));
    }

    /**
     * Возвращает пластинку авторизованного пользователя или выводит страницу 404
     *
     * @param int $recordId
     *
     * @return Record
     */
    private function getUserRecord(int $recordId)
    {
        $record = Record::find($recordId);

        //Не даём редактировать чужие пластинки
        if ($record->user_id != Auth::id()) {
            abort(404, Lang::get('record.not found'));
        }

        return $record;
    }
}
