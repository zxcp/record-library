<?php

namespace App\Http\Requests;

class UpdateRequest extends CreateRequest
{
    public function rules()
    {
        return array_merge(parent::rules(), ['recordId' => 'required|int']);
    }
}
