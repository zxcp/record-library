<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/list', 'RecordController@list')->name('list');
Route::get('/new', 'RecordController@createForm')->name('new');
Route::get('/delete', 'RecordController@delete')->name('delete');
Route::get('/edit', 'RecordController@edit')->name('edit');

Route::post('/create', 'RecordController@create')->name('create');
Route::post('/update', 'RecordController@update')->name('update');
